Bing Custom Search Engine
=========================

Drupal integration with the Bing Custom Search Engine.


Installation
------------

First you will need to set up a Bing Custom Search engine and get API credentials.

The best place to start is here: https://docs.microsoft.com/en-us/azure/cognitive-services/bing-custom-search/quickstart


Hooks
-----

To alter the search results before output, do the following:

/**
 * Implements hook_bcse_result_alter().
 */
function mymodule_bcse_result_alter(&$row) {
  $row['new_field'] = 'Arbitrary value';
}


Theming
-------

To override the display, copy either of the templates into your theme directory.

templates/bcse-result.tpl.php
templates/bcse-results.tpl.php
