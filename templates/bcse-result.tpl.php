<?php

/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * bcse-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result.
 *
 * @see template_preprocess_bcse_result()
 */
 ?>
<li class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="bcse-title"<?php print $title_attributes; ?>>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h3>
  <?php if ($url) : ?>
      <p class="bcse-search-url"><a href="<?php print $url; ?>"><?php print $url; ?></a></p>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div class="bcse-search-snippet-info">
    <?php if ($snippet) : ?>
      <p class="bcse-search-snippet"<?php print $content_attributes; ?>><?php print $snippet; ?></p>
    <?php endif; ?>
  </div>

</li>
