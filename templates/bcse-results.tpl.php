<?php

/**
 * @file
 * Search-results.tpl.php
 * Default theme implementation for displaying search results.
 * This template collects each invocation of theme_bcse_result(). This and
 * the child template are dependant to one another sharing the markup for
 * definition lists.
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $pagerstart: The result number of the first result on this particular page.
 * @see template_preprocess_bcse_results()
 */
?>
<span><?php print $head; ?></span>
<ol class="bing-search-results" start="<?php print $pagerstart; ?>">
  <?php print $search_results; ?>
</ol>
<?php print $pager; ?>
