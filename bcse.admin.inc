<?php

/**
 * @file
 * BCSE admin functions.
 */

/**
 * Bing Custom Search Engine configuration function.
 */
function bcse_admin() {
  return bcse_settings();
}

/**
 * Module settings.
 */
function bcse_settings() {
  $form = array();

  $form['bcse'] = array(
    '#title' => t('Bing Custom Search Engine'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['bcse']['main'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main'),
    '#weight' => -10,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['bcse']['main']['bcse_primary_key'] = array(
    '#title' => t('Bing Primary Key'),
    '#type' => 'textfield',
    '#description' => t('Your Bing Custom Search primary key. Your search will not work without this key.'),
    '#default_value' => variable_get('bcse_primary_key', ''),
    '#required' => TRUE,
  );
  $form['bcse']['main']['bcse_custom_config'] = array(
    '#title' => t('Bing Custom Config Number'),
    '#type' => 'textfield',
    '#description' => t('The custom config number found in your API endpoint example.'),
    '#default_value' => variable_get('bcse_custom_config', ''),
    '#required' => TRUE,
  );
  $form['bcse']['language'] = array(
    '#type' => 'fieldset',
    '#title' => t('Language'),
    '#weight' => -9,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['bcse']['language']['bcse_language'] = array(
    '#title' => t('Language'),
    '#type' => 'textfield',
    '#description' => t('The language to search. Write the language code with two letters.<br />Ex: for "english", input "en-us"; for "Chinese (Simplified)", input "zh-CN".'),
    '#default_value' => variable_get('bcse_language', 'en-us'),
    '#size' => 5,
    '#max_length' => 5,
    '#required' => TRUE,
  );
  $form['bcse']['miscellaneous'] = array(
    '#type' => 'fieldset',
    '#title' => t('Miscellaneous'),
    '#weight' => -8,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['bcse']['miscellaneous']['bcse_results_per_page'] = array(
    '#title' => t('Search Results Per Page'),
    '#type' => 'select',
    '#options' => array(
      '5' => t('5'),
      '10' => t('10'),
      '20' => t('20'),
    ),
    '#description' => t('How many items should the search results show?'),
    '#default_value' => variable_get('bcse_results_per_page', '20'),
  );
  $form['bcse']['miscellaneous']['bcse_pager_size'] = array(
    '#title' => t('Pager Size'),
    '#type' => 'select',
    '#options' => array(
      '0' => t('0'),
      '5' => t('5'),
      '10' => t('10'),
      '20' => t('20'),
    ),
    '#description' => t('How many pages of results to show in the pager?'),
    '#default_value' => variable_get('bcse_pager_size', '10'),
  );
  $form['bcse']['miscellaneous']['bcse_safesearch'] = array(
    '#title' => t('Safe Search Level'),
    '#type' => 'select',
    '#options' => array(
      'Off' => t('Off'),
      'Moderate' => t('Moderate'),
      'Strict' => t('Strict'),
    ),
    '#description' => t('What level of safe search would you like to use?'),
    '#default_value' => variable_get('bcse_safesearch', 'Moderate'),
  );

  return system_settings_form($form);

}
