<?php

/**
 * @file
 * Drupal integration for the Bing Custom Search Engine.
 */

/**
 * Implements hook_theme().
 */
function bcse_theme() {
  return [
    'bcse_result' => [
      'variables' => ['result' => NULL, 'module' => NULL],
      'file' => 'bcse.theme.inc',
      'template' => 'templates/bcse-result',
    ],
    'bcse_results' => [
      'variables' => ['results' => NULL, 'module' => NULL, 'context' => NULL],
      'file' => 'bcse.theme.inc',
      'template' => 'templates/bcse-results',
    ],
  ];
}

/**
 * Implements hook_permission().
 */
function bcse_permission() {
  return [
    'administer bing search' => [
      'title' => t('Administer Bing Custom Search Engine'),
    ],
    'access bing custom search engine' => [
      'title' => t('Access Bing Custom Search Engine'),
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function bcse_menu() {
  $items['admin/config/search/bcse'] = [
    'title' => 'Bing Custom Search Engine',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['bcse_admin'],
    'access arguments' => ['administer bing search'],
    'type' => MENU_NORMAL_ITEM,
    'description' => 'Bing Custom Search settings',
    'file' => 'bcse.admin.inc',
  ];

  $items['bcse/search'] = [
    'title' => 'Search Results',
    'page callback' => 'bcse_results_page',
    'access arguments' => ['access bing custom search engine'],
    'type' => MENU_NORMAL_ITEM,
  ];

  return $items;
}

/**
 * Implements hook_block_info().
 */
function bcse_block_info() {
  $blocks = [];

  $blocks['bcse'] = [
    'info' => t('Bing Custom Search Engine Search Box'),
    'cache' => DRUPAL_NO_CACHE,
  ];

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function bcse_block_view() {
  $form = drupal_get_form('bcse_form');
  $block = [
    'subject' => t('Bing Custom Search Form'),
    'content' => $form,
  ];

  return $block;
}

/**
 * Implements hook_form().
 */
function bcse_form($form, &$form_state) {

  // If we are on the bcse search page, populate the search box
  // with the term we're searching for currently.
  if (arg(0) == 'bcse' && arg(1) == 'search') {
    $s = arg(2);
  }

  $form = [];

  $form['search_term'] = [
    '#type' => 'textfield',
    '#size' => 40,
  ];
  if (!empty($s)) {
    $form['search_term']['#default_value'] = $s;
  }
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Search'),

  ];

  $action = 'bcse/search';
  $form['#action'] = url($action);
  // Record the $action for later use in redirecting.
  $form_state['action'] = $action;
  return $form;
}

/**
 * Implemets hook_form_submit().
 */
function bcse_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'bcse/search/' . $form_state['values']['search_term'];
}

/**
 * Implements hook_results_page().
 */
function bcse_results_page() {
  if (!empty(arg(2))) {
    $params = drupal_get_query_parameters();

    $currentpage = 0;
    if (!empty($params['page'])) {
      $currentpage = $params['page'];
    }

    $data = [
      'q' => arg(2),
      'customconfig' => variable_get('bcse_custom_config'),
      'count' => variable_get('bcse_results_per_page'),
      // 'count' => 14,.
      'offset' => $currentpage * variable_get('bcse_results_per_page'),
      'mkt' => variable_get('bcse_language'),
      'safesearch' => variable_get('bcse_safesearch'),
    ];

    $options = [
      'method' => 'GET',
      // 'data' => drupal_http_build_query($data),
      'timeout' => 15,
      'headers' => ['Ocp-Apim-Subscription-Key' => variable_get('bcse_primary_key')],
    ];

    $url = 'https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search';
    $full_url = url($url, ['query' => $data]);
    $result = drupal_http_request($full_url, $options);

    $json = drupal_json_decode($result->data);
  }

  // Calling all modules implementing hook_bcse_search_data_alter():
  drupal_alter('bcse_search_data', $data);

  // Build output.
  if (!empty($json['webPages'])) {
    $output = [];
    // Build pager info.
    $pageinfo = [];
    $pageinfo['pagesize'] = variable_get('bcse_results_per_page');
    $pageinfo['pagersize'] = variable_get('bcse_pager_size');
    $pageinfo['totalresults'] = $json['webPages']['totalEstimatedMatches'];
    $pageinfo['currentpage'] = 1;
    if (!empty($params['page'])) {
      $pageinfo['currentpage'] = $params['page'] + 1;
    }
    $pageinfo['show_start'] = ($pageinfo['pagesize'] * $pageinfo['currentpage']) - $pageinfo['pagesize'] + 1;
    $pageinfo['show_end'] = ($pageinfo['pagesize'] * $pageinfo['currentpage']);

    if ($pageinfo['show_end'] > $pageinfo['totalresults']) {
      $pageinfo['show_end'] = $pageinfo['totalresults'];
    }

    if (isset($json['queryContext'])) {
      $pageinfo['query_context'] = $json['queryContext'];
    }

    // Loop results.
    foreach ($json['webPages']['value'] as $result) {
      $output_row = [
        'link' => urldecode($result['url']),
        'title' => $result['name'],
        'snippet' => $result['snippet'],
      ];
      // Call out hooks.
      drupal_alter('bcse_result', $output_row);
      $output[] = $output_row;
    }
  }
  else {
    $output[] = [
      'link' => '',
      'title' => '',
      'snippet' => t('No results'),
    ];
    $output['head'] = '';
    $output['pager'] = '';
    $output['pagerstart'] = '';
    return theme('bcse_results', ['results' => $output]);
  }

  $output['head'] = bcse_get_head($pageinfo);
  $output['pager'] = bcse_get_pager($pageinfo);
  $output['pagerstart'] = $pageinfo['show_start'];

  return theme('bcse_results', [
    'results' => $output,
    'context' => isset($json['queryContext']) ? $json['queryContext'] : NULL
  ]);
}

/**
 * Getting search heading.
 *
 * @param array $pageinfo
 *   Information returned from search
 *
 * @return string
 *   HTML output.
 */
function bcse_get_head($pageinfo) {

  $html = '<div class="searchhead">';

  if (isset($pageinfo['query_context']['alteredQuery'])) {
    $html .= '<div class="altered-query">';
    $html .= t('Including results for <strong>@altered_query</strong>.', [
        '@altered_query' => $pageinfo['query_context']['alteredQuery'],
      ]
    );
    $html .= '<br />';

    $html .= t('Do you want results only for <a href="@query_url">@original_query</a>?', [
        '@query_url' => '/bcse/search/' . urlencode($pageinfo['query_context']['alterationOverrideQuery']),
        '@original_query' => $pageinfo['query_context']['originalQuery'],
      ]
    );

    $html .= '</div>';
  }

  if (variable_get('bcse_results_per_page') > 1 && $pageinfo['totalresults'] !== 0) {
    $html .= '<div class="result-count">';
    $html .= t('Displaying @show_start - @show_end of approximately @totalResults results.', [
        '@show_start' => $pageinfo['show_start'],
        '@show_end' => $pageinfo['show_end'],
        '@totalResults' => $pageinfo['totalresults'],
      ]
    );
    $html .= '</div>';
  }

  $html .= '</div>';

  return $html;
}

/**
 * Get a pager for the search results.
 *
 * @return string
 *   The pager HTML code.
 */
function bcse_get_pager($pageinfo) {

  $currenturl = current_path();
  $params = drupal_get_query_parameters();
  $s = arg(2);

  $html = "<div class=\"item-list bing-custom-search-engine-pager\"><ul class=\"pager\">\n";
  $totalpages = $pageinfo['totalresults'] / $pageinfo['pagesize'] + 1;

  // If we're on the second page or more, add a link to the first page.
  if ($pageinfo['currentpage'] > 1) {
    $link_page = $s . '?' . drupal_http_build_query(['page' => 0]);
    $html .= "\t<li class=\"pager-first\"><a href=\"{$link_page}\">" . t('‹‹ first') . "</a></li>\n";
  }

  // If we're on the second page or more, add a link to the previous page.
  if ($pageinfo['currentpage'] > 1) {
    $link_page = $s . '?' . drupal_http_build_query(['page' => $pageinfo['currentpage'] - 1]);
    $html .= "\t<li class=\"pager-previous\"><a href=\"{$link_page}\">" . t('‹ previous') . "</a></li>\n";
  }

  // Defines the start and finish of the pager.
  $pagerstart = $pageinfo['currentpage'] - (round($pageinfo['pagersize'] / 2) - 1);
  $pagerfinish = $pageinfo['currentpage'] + (round($pageinfo['pagersize'] / 2) - 1);

  // Start values, if $pagerstart is negative.
  if ($pagerstart < 1) {
    $pagerstart = 1;
    $pagerfinish = $pageinfo['pagesize'] > $totalpages ? $totalpages : $pageinfo['pagesize'];
  }

  // Limit $pagerfinish to the same as $totalpages.
  $pagerfinish = $pagerfinish > $totalpages ? $totalpages : $pagerfinish;

  if ($pagerstart > 1) {
    $html .= "\t<li class=\"pager-ellipsis\">...</li>\n";
  }

  // Loop through the pages and give them links. The pager starts at 0, but
  // the display numbers should start at 1. This makes it easier to create
  // the offset query to get results.
  for ($i = $pagerstart; $i <= $pagerfinish; $i++) {
    if ($i != $pageinfo['currentpage']) {
      $pagernumber = $i - 1;
      $link_page = $s . '?' . drupal_http_build_query(['page' => $pagernumber]);
      $html .= "\t<li class=\"pager-item\"><a href=\"{$link_page}\">{$i}</a></li>\n";
    }
    // If we're on looping through the current page, don't give it a link,
    // just give it the active class.
    else {
      $html .= "\t<li class=\"pager-current\">{$i}</li>\n";
    }
  }

  if ($pagerfinish < $totalpages) {
    $html .= "\t<li class=\"pager-ellipsis\">...</li>\n";
  }

  // If we're not on the last page, produce a "next" link. Bing API returns
  // an estimated total results, so if we had a skip to end link it may overrun
  // the actual total results. Therefore, we will only
  // have a "next" link, not a "last" link.
  if ($pageinfo['currentpage'] < intval($totalpages)) {
    $link_page = $s . '?' . drupal_http_build_query(['page' => $pageinfo['currentpage']]);
    $html .= "\t<li class=\"pager-next\"><a href=\"{$link_page}\">" . t('next ›') . "</a></li>\n";
  }

  // Close ol.
  $html .= "</ul></div>\n";

  return $html;
}
