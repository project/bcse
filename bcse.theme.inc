<?php

/**
 * @file
 * Themeable functions for Bing Custom Search Engine.
 */

/**
 * Process variables for bcse-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $module.
 *
 * @see bcse-result.tpl.php
 */
function template_preprocess_bcse_result(&$variables) {
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = $result['title'];
  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';
}

/**
 * Process variables for bcse-results.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $module.
 *
 * @see bcse-results.tpl.php
 */
function template_preprocess_bcse_results(&$variables) {
  $results = $variables['results'];
  $variables['pagerstart'] = $results['pagerstart'];
  unset($results['pagerstart']);
  $variables['head'] = $results['head'];
  unset($results['head']);
  $variables['pager'] = $results['pager'];
  unset($results['pager']);
  $variables['search_results'] = '';
  foreach ($results as $entry) {
    $variables['search_results'] .= theme('bcse_result', array('result' => $entry));
  }
}
